import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String[] args) {
        String input = "I AM AM LEARNING JAVA JAVA";
        Map<String, Integer> hm = m1(input);
        hm.forEach((k,v)-> System.out.println(k+":::"+v));
    }

    private static Map<String,Integer> m1(String str) {
        Map<String,Integer> hm = new HashMap<>();
        String[] words = str.split(" ");
        for (String word : words) {
            if(hm.get(word)!=null){
                hm.put(word,hm.get(word)+1);
            }else{
                hm.put(word,1);
            }
        }
        return hm;
    }
}
