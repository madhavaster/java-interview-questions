# Java Interview Questions - String 
### Question :

---
Find and display the number of duplicate occurances of each 

Task 1) word

Task 2) character

of a string

```java
public class Test{
    public static void main(String...args){
        String str = "I AM AM LEARNING JAVA JAVA";
    }
}
```
### Solution:

---
Steps 
1) Split each word using split method. i.e. {"I","AM","AM","LEARNING","JAVA","JAVA"}
2) Use a map to store them as k,v where k is String, v is Integer
3) Iterate through the array using foreach loop and add word and count to the hashmap.

```java
import java.util.HashMap;

public class Test {
    public static void main(String[] args) {
        String input = "I AM AM LEARNING JAVA JAVA";
        m1(input);
    }

    static void m1(String str) {
        Map<String, Integer> hm = new HashMap<>();
        String[] words = str.split(" ");
        for (String word : words) {
            if(hm.get(word)!=null){
                hm.put(word,hm.get(word)+1);
            }else{
                hm.put(word,1);
            }
        }
        System.out.println(hm);
    }
}
```