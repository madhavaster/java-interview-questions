public class Test {
    static int a = 1111;
    static {
        a = a-- - --a;
    }
    {
        a=a++ + ++a;
    }

    public static void main(String[] args) {
        //the line below this gives the output
        //\u000d System.out.println("comment executed");


     /*   String s = "ONE"+1+2+"TWO"+"THREE"+3+4+"FOUR"+"FIVE"+5;
        System.out.println(s);*/

        int i = 10 + +11 - -12 + +13 - -14 + +15;
        System.out.println(i);

        String one = "Random";
        String two = "RAndom";
        if(one == two){
            System.out.println("one==two");
        }else{
            System.out.println("one!=two");
        }

        System.out.println(Fruit.TYPE);

        final class Constants{
            public final static String name = "PI";
        }
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Constants.name);
            }
        });
      //  t.start();

        Integer i1 = 127;
        Integer i2 = 127;
        System.out.println(i1==i2);
        Integer i3 = 128;
        Integer i4 = 128;
        System.out.println(i3==i4);

        System.out.println(Math.min(Integer.MIN_VALUE,0.0d));

        long longWithL = 1000 * 60 * 60 * 24 * 365L;
        long longWithoutL = 1000 * 60 * 60 * 24 * 365;
        System.out.println(longWithL);
        System.out.println(longWithoutL);

        System.out.println(a);
        try{
             Fruit fruit = (Fruit)Class.forName("Fruit").newInstance();
             fruit.m2();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        Test2 test2 = new Test2();
        int a = test2.a;
        System.out.println(a);
    }
}
interface IFruit{
    String TYPE = "Apple";
}
class Fruit implements IFruit{
    void m2(){
        System.out.println("inside m2 method");
    }
}