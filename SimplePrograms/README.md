# Java Interview Questions
## Getting started
Here are the 10 most commonly asked programming questions asked in the Technical round of Interview in any IT company!!
### Question # 1

```java
public class Test{
    public static void main(String...args){
        // the line below this gives an output
        //\u000d System.out.println("comment executed");
    }
}
```
Answer: \u000d is a unicode character for newline. So after compilation the above code looks as below.
```java
public class Test{
    public static void main(String...args){
        // the line below this gives an output
        //
        Systtem.out.println("comment executed");
    }
}
```
so output is: sop gets printed.

### Question # 2

```java
public  class Test{
    public static void main(String[] args) {
        String s = "ONE"+1+2+"TWO"+"THREE"+3+4+"FOUR"+"FIVE"+5;
        System.out.println(s);
    }
}
```
output is : ONE12TWOTHREE34FOURFIVE5

### Question # 3
```java
public class Test{
    public static void main(String[] args) {
        int i = 10 + +11 - -12 + +13 - -14 + +15;
        System.out.println(i);
    }
}
```
output is: 75 
Hint: int i = 10 + (+11) - (-12) + (+13) - (-14) + (+15); // unary operators
### Question # 4
```java
public class Test{
    public static void main(String[] args) {
        String one = "Random";
        String two = "RAndom";
        if(one == two){
            System.out.println("one==two");
        }else{
            System.out.println("one!=two");
        }
    }    
}
```
Output: else block gets executed. Reason being == is meant for reference comparision.
### Question # 5
```java
interface IFruit{
    public String TYPE = "Apple";
}
class Fruit implements IFruit{
    
}
public class Test{
    public static void main(String[] args) {
        System.out.println(Fruit.TYPE);
    }
}
```
Hints: 
1) By default a variable in an interface is public static final. So variable TYPE is public static final by default
2) Fruit class is implementing IFruit, so using Fruit class we can access static members without creating object. 
3) So we can call it.
Output is "Apple"

### Question # 6

```java
public class Test {
    public static void main(String[] args) {
        final class Constants {
            public static String name = "PI";
        }
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Constants.name);     
            }
        });
        t.start();
    }
}
```
Hints: 
1) We have class Constants in the class Test. So Constants class is called nested class.
2) Inside nested class, the field name cannot be declared static in a non-static class type. 
3) Output is compilation error. 

### Questino # 7
```java
public class Test{
    public static void main(String[] args) {
        Integer i1 = 127;
        Integer i2 = 127;
        System.out.println(i1==i2);
        Integer i3 = 128;
        Integer i4 = 128;
        System.out.println(i3==i4);
    }
}
```
Hint:
Integer caching happens between -128 to 127. So output is true,false.
Note: Integer caching concept is introduced in java 5.

### Question # 8
```java
public class Test{
    public static void main(String[] args) {
        System.out.println(Math.min(Double.MIN_VALUE,0.0d));
    }
}
```
Hint: Double.MIN_VALUE is a constant holding the smallest positive nonzero value of type double (4.9e-324), which is
greater than 0.0d, so output is 0.0d.
For Integer.MIN_VALUE , it is negative number.

### Question # 9
```java
public class Test{
    public static void main(String[] args) {
      long longWithL = 1000 * 60 * 60 * 24 * 365L;
      long longWithoutL = 1000 * 60 * 60 * 24 * 365;
        System.out.println(longWithL);
        System.out.println(longWithoutL);
    }
}
```
Hint: longWithoutL will hold the truncated value as there is no L , its treated as integer. 

### Question # 10
```java
public class Test{
    static int a = 1111;
    static{
        a = a-- - --a;
    }
    {a = a++ + ++a;}
    public static void main(String[] args) {
        System.out.println(a);            
    }
}
```
Hint: a-- means 1111 only and then it becomes 1110 , after than --a means 1109.
so the difference is 1111-1109 = 2 is the answer. the other block doesnt get executed at all. 

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/madhavaster/java-interview-questions.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://docs.gitlab.com/ee/user/clusters/agent/)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!).  Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:e0bda05d170cf002ed7bed637542e3e8?https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

