# Java Interview Questions 
## Question #1 
### What are the different ways we can break a singleton design pattern in java?
### Answer: 
1) Reflection
2) Serialization
3) Cloning
4) By Executor Service

## Question #2
### Difference between NoClassDefFoundError and ClassNotFoundException?
```java
public class ClassNotFoundExceptionDemo{
    public static void main(String[] args) {
        try{
            Class.forName("com.mysql.jdbc.Driver");
        }catch (ClassNotFoundException exception){
            exception.printStackTrace();
        }
    }
}
```
Answer: If com.mysql.jdbc.Driver class is not available in the classpath, we will get ClassNotFoundException at runtime but not at compile time.
```java
public class NoClassDefFoundErrorDemo{
    public static void main(String[] args) {
        A a = new A();
    }
}
```
Answer: A.java is there but A.class is removed from bin location. In this case, we will get NoClassDefFoundError.

## Question #3
### Apart from String, what else predefined classes we can use as keys in a map?
Answer: Integer and other wrapper classes like java.lang.Integer, java.lang.Byte, java.lang.Character, java.lang.Short, java.lang.Boolean, java.lang.Long, java.lang.Double, java.lang.Float are natural candidates of HashMap key.

## Question #4
### Given an employee list, sort employees based on their salaries in desc order?
Answer: using streams api.

